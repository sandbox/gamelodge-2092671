The Iframe Field module provides a new field type called Iframe_field
Use this to add iframe fields to any fielad-able entity.

The iframe height/url/width and scrolling can be set via a widget.